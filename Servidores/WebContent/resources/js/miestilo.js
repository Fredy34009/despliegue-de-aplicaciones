
function vista3() {

	swal(
			{
				title : "Pago",
				html : true,
				text : "<form><div style='border-color: red;border: 1px solid #000000;margin: 10px;padding: 25px;-moz-border-radius: 9px;-webkit-border-radius:9px;'><h6>Datos de usuario</h6><label class='float-left'>Nombre</label><input id='name' class='form-control' />"
						+ "<label class='float-left'>Correo</label><input id='correo' class='form-control' />"
						+ "<label class='float-left'>Codigo Postal</label><input id='postal' class='form-control' /></div><div style='border-color: red;border: 1px solid #000000;margin: 10px;padding: 25px;-moz-border-radius: 9px;-webkit-border-radius:9px;'><h6>Forma de pago</h6>"
						+ "<label class='float-left' for='pag'>Tipo de pago</label><select id='pag' class='form-control'><option value='1'>Tarjeta de credito</option>"
						+ "<option value='2'>Efectivo</option>"
						+ "<option value='3'>Tarjeta de regalo</option>"
						+ "</select></div></form>",
				type : "info",
				showCancelButton : true,
				confirmButtonClass : "btn btn-success",
				cancelButtonClass : "btn btn-danger",
				cancelButtonText : "Volver",
				confirmButtonText : "Comprar",
				closeOnConfirm : false,
				closeOnCancel : false,
			}, function(isConfirm) {
				if (isConfirm) {
					swal("Compra Exitosa", "Has realizado tu compra",
							"success");
				} else {
					swal("Cancelado", "Has cancelado tu compra", "error");
				}
			});
}